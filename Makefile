main: start
up: start
up.www: start.www
up.api: start.api

prepare:
	@echo "Installing tools"
	npm i -g @adonisjs/cli yarn cross-env
	@echo "Installing dependencies Api"
	cd ./api && yarn && cp .env.example .env && adonis key:generate
	@echo "Installing dependencies www"
	cd ./www && yarn

start.www:
	@echo "Run www frontend service"
	cd ./www && yarn dev

start.api:
	@echo "Run api backend service"
	cd ./api && adonis serve --dev

