'use strict'

const Messages = use('App/Static/Message')
const { validate } = use('Validator')

class MessageController {
  constructor() {
    this.messages = Messages
  }
  index({ response }) {
    const messages = this.messages.map(message => ({ ...message, msgLength: message.message.length }))
    return response.json({ messages })
  }

  async store({ request, response }) {
    const rules = {
      'message': 'required|min:3'
    }

    const messages = {
      'message.required': 'Please provide the {{ field }} ',
      'message.min': 'Please provide the {{ field }} at least {{ argument.0 }} charactors'
    }

    const validation = await validate(request.all(), rules, messages)

    if (validation.fails()) {
      return response.status(400).json(validation.messages())
    }

    const message = request.all()
    const id = this.messages.length + 1
    this.messages.push({ ...message, id })

    return response.json({ ...message, msgLength: message.message.length, id })
  }
}

module.exports = MessageController
