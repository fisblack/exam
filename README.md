# WORKSHOP EXAM

## เริ่มใช้งาน

## สำหรับเครื่อง mac
1) สั่ง `make prepare` เพื่อตั้งเครื่องมือและ Dependencies ทั้งหมด
2) สั่ง `make start.api` เพื่อให้ API ทำงาน
3) สั่ง `make start.www` เพื่อให้ Frontend สำหรับหน้าบ้านทำงาน

## สำหรับ manual

### เตรียมเครื่องมือ
```console
  npm i -g @adonisjs/cli yarn cross-env
```
### API
```console
  cd ./api
  yarn
  cp .env.example .env
  adonis key:generate
  adonis serve --dev
```

### Frontend
```console
  cd ./www
  yarn
  yarn dev
```

## API ENDPOINT
- GET http://localhost:3333/api/message
- POST http://localhost:3333/api/message require {message: String}